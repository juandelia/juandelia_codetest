package com.agilengine.quiz.codetest.model;

import lombok.Data;

import java.util.Map;

@Data
public class MatchCriteria {

  private Map<String, String> attributes;

  private String content;

  private String tagName;

}
