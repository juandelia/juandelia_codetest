package com.agilengine.quiz.codetest.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class RankedMatch {

  private String xpath;

  private List<String> matchingAttrs;

}
