package com.agilengine.quiz.codetest.service;

import com.agilengine.quiz.codetest.model.RankedMatch;
import com.agilengine.quiz.codetest.model.MatchCriteria;
import com.agilengine.quiz.codetest.model.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MatchService {

  private static final int MINIMUM_SIMILARITY = 3;

  private static final String ELEMENT_CONTENT_ATTR_NAME = "elementContent";

  private ApplicationArguments applicationArguments;

  private ParserService parserService;

  private String originalElementId = "make-everything-ok-button";

  @Autowired
  public MatchService(ApplicationArguments applicationArguments, ParserService parserService) {
    this.applicationArguments = applicationArguments;
    this.parserService = parserService;
  }

  /**
   * Main Entry point to trigger the best matching element search
   */
  @PostConstruct
  public void findBestMatchingElement() {
    List<String> args = applicationArguments.getNonOptionArgs();

    try {
      if (args.size() < 2) {
        throw new ServiceException("You need to pass at least 2 parameters: originalFile, newFile and optionally a third with the element id to match");
      }

      if (args.size() > 2) {
        originalElementId = args.get(2);
      }

      String result = getBestMatchingElement(args.get(0), args.get(1));
      System.out.println(result);

    } catch (ServiceException e) {
      System.out.println(e.getMessage());
      log.error(e.getMessage(), e);
    } catch (Exception e) {
      log.error("There was an unexpected error", e);
      System.out.println("There was an unexpected error");
      e.printStackTrace();
    }
  }

  /**
   * Finds the location of the best matching element on the diffFile if any
   *
   * @param originalFile file containing the original element to be compared
   * @param diffFile     file to get similar elements to the original
   * @return path to the best matching element on the diff file
   */
  private String getBestMatchingElement(String originalFile, String diffFile) {

    MatchCriteria matchCriteria = createRequestElementCriteria(originalFile);

    List<RankedMatch> matchingRanking = getMatchingRanking(diffFile, matchCriteria);

    String result = "No matches were found";

    if (!matchingRanking.isEmpty()) {
      RankedMatch bestRankedMatch = matchingRanking.get(0);

      int similaritiesCount = bestRankedMatch.getMatchingAttrs().size();

      if (similaritiesCount < MINIMUM_SIMILARITY) {
        result = "No matches were found having at least " + MINIMUM_SIMILARITY
                + " similar properties to the original element " + originalElementId + " (best match found with " + similaritiesCount + ")";
      } else {
        result = "**Best mathcing element is ** " + bestRankedMatch.getXpath() + " ** with "
                + similaritiesCount + " similar properties: " + bestRankedMatch.getMatchingAttrs();
      }

    }

    return result;
  }

  /**
   * Generates a ranking of partial matching elements based on its similarities with the requested original element
   *
   * @param diffFile      changed file where to look for same elements to the original
   * @param matchCriteria matching criteria of elements
   * @return a List ordered by similarity with the xpath and matching elements same attributes
   */
  private List<RankedMatch> getMatchingRanking(String diffFile, MatchCriteria matchCriteria) {
    String cssQuery = matchCriteria.getTagName();
    List<RankedMatch> ranking = new ArrayList<>();

    log.info("getMatchingRanking:: Looking for similar elements to: " + cssQuery);
    Optional<Elements> elementsOpt = parserService.findElementsByQuery(new File(diffFile), cssQuery);


    if (elementsOpt.isPresent()) {
      ranking = elementsOpt.get().stream().map(element -> {
        List<String> matchingAttrs = new ArrayList<>();
        element.attributes().forEach(attribute -> {
          String foundAttributeValue = matchCriteria.getAttributes().get(attribute.getKey());
          if (attribute.getValue().equals(foundAttributeValue)) {
            matchingAttrs.add(attribute.getKey() + "=" + attribute.getValue());
          }
        });

        if (element.ownText().equals(matchCriteria.getContent())) {
          matchingAttrs.add(ELEMENT_CONTENT_ATTR_NAME + "=" + element.ownText());
        }

        return new RankedMatch(element.cssSelector(), matchingAttrs);

      }).sorted((rm1, rm2) -> Long.compare(rm2.getMatchingAttrs().size(), rm1.getMatchingAttrs().size()))
              .collect(Collectors.toList());
    }

    return ranking;

  }


  /**
   * Creates a criteria considering the original element attributes to be compared
   *
   * @param originalFilePath file where to look for the element by its id
   * @return a MatchCriteria object with the values to compare
   */
  private MatchCriteria createRequestElementCriteria(String originalFilePath) {

    String cssQuery = "*[id=\"" + originalElementId + "\"]";

    Optional<Elements> elementsOpt = parserService.findElementsByQuery(new File(originalFilePath), cssQuery);

    Optional<MatchCriteria> requestElementOptional = elementsOpt.map(originalElementIdMatch ->
            {
              Map<String, String> attributes = new HashMap<>();
              MatchCriteria matchCriteria = new MatchCriteria();

              if (originalElementIdMatch.size() == 0) {
                throw new ServiceException("No match found with the given element id");
              }

              if (originalElementIdMatch.size() > 1) {
                throw new ServiceException("More than one match found with the given element id");
              }

              originalElementIdMatch.iterator().forEachRemaining(matchingEl -> {
                matchingEl.attributes().asList().stream()
                        .forEach(attr -> attributes.put(attr.getKey(), attr.getValue()));

                matchCriteria.setContent(matchingEl.ownText());
                matchCriteria.setTagName(matchingEl.tagName());
                matchCriteria.setAttributes(attributes);
              });

              return matchCriteria;
            }
    );

    return requestElementOptional.get();
  }


}
