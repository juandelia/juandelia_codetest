Smart XML Analyzer code test by Juan Delía

This application tries to find the best match between a passed element id from the original document, in the new version of the document passed as second param.

It also supports the changing of the original element id, by passing a third parameter.

**Assumed scenario

The new location of the element could be anywhere on the new file. The similarity will be around its own properties but location (position in the document) will not be taken into account.

The weight of the element's properties and the content of the element, will all be the same (i.e. same class will count as much as the same onclick).

The minimum similarity for a element to be considered the same, is one equal property (Elements with more similar properties than others will be ranked better).

**Run example

java -jar codetest.jar "C:\tests\sample-0-origin.html" "C:\tests\sample-2-container-and-clone.html" make-everything-ok-button

**Sample Output

**Best mathcing element is ** #page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-body > div.some-container > a.btn.test-link-ok ** 
with 4 similar properties: [href=#ok, title=Make-Button, rel=next, htmlContent=Make everything OK]


In the output there will be displayed:

*The best match fountd xpath

*The number of matching attributes (including the inner element text)

*The name and value of the matching attributes


**Sample diff files output

sample-1-evil-gemini.html

**Best mathcing element is ** #page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-body > a.btn.btn-success ** with 4 similar properties: [class=btn btn-success, title=Make-Button, onclick=javascript:window.okDone(); return false;, elementContent=Make everything OK]

sample-2-container-and-clone.html

**Best mathcing element is ** #page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-body > div.some-container > a.btn.test-link-ok ** with 4 similar properties: [href=#ok, title=Make-Button, rel=next, elementContent=Make everything OK]


sample-3-the-escape.html

**Best mathcing element is ** #page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-footer > a.btn.btn-success ** with 4 similar properties: [class=btn btn-success, href=#ok, rel=next, onclick=javascript:window.okDone(); return false;]


sample-4-the-mash.html

**Best mathcing element is ** #page-wrapper > div.row:nth-child(3) > div.col-lg-8 > div.panel.panel-default > div.panel-footer > a.btn.btn-success ** with 4 similar properties: [class=btn btn-success, href=#ok, title=Make-Button, rel=next]



Im really sorry for the lack of tests, I should have added some, but ran out of time